const { guardarInfo } = require('./guardarDB');
const Persona = require('./persona')

class Personas {

  constructor() {
    this._listado = [];
  } 

  crearPersona(persona = {}) {
    // console.log(persona);

    this._listado.push(persona)
    // console.log(this._listado, '9999');SI
    // this._listado[person.id] = person
    // console.log('persona', person, 'persona'); //not defined
  }

  get personasArr() {
    const listado = [];

    Object.keys(this._listado).forEach(key => {

      const person = this._listado[key];
      listado.push(person);
    });

    return listado;
}

cargarTareasFormArray(personas = []){

  personas.forEach(persona => {
    this._listado[persona.id] = persona;
  }) 
} 

editarPersona(persona, index) {
  this._listado[index].nombres = persona.nombres,
  this._listado[index].apellidos = persona.apellidos,
  this._listado[index].ci = persona.ci,
  this._listado[index].direccion = persona.direccion,
  this._listado[index].sexo = persona.sexo
  guardarInfo(this._listado)
}

eliminarPersona(id){
  
  this._listado = this._listado.filter(data =>  {
    return data.id!== id
  })
}


}

module.exports = Personas;