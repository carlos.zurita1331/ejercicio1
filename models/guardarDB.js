
const fs = require('fs');
const archivo = './db/data.json';

const guardarInfo = (data) => {

  fs.writeFileSync(archivo, JSON.stringify(data))
  //convierte en cadena la data
}

const leerInfo = () => {
  if(!fs.existsSync(archivo)){
    return null;
   }
   
   const info = fs.readFileSync(archivo, {encoding: 'utf-8'})
  //  console.log('ifo', info, 'info');
   //contenido es array de json en string

   const data = JSON.parse(info);

  //  console.log(data, 'data ');
   return data

}


module.exports = {
  guardarInfo,
  leerInfo
};