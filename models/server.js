const express = require('express');

class Server {

  constructor(){
    this.app = express();
    this.port = process.env.PORT;

    this.personaPath = '/api/persona';

    this.middlewares();

    this.routes();
  }
 
  middlewares() {
    this.app.use(express.json());

    this.app.use(express.static('public'));
  }

   routes() {
     this.app.use(this.personaPath, require('../routes/personaRoutes'));

  }

   listen() {
    //Con process.env.PORT obtenemos el valor de la variable PORT 
    this.app.listen(this.port, () => {
    console.log('Servidor corriendo en puerto ', this.port);
    });
  }

}

module.exports = Server;