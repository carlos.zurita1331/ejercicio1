const { request } = ("express");
const req = require("express/lib/request");
const { leerInfo, guardarInfo } = require("../models/guardarDB");
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const { response } = 'express';

const personas = new Personas(); //lista


const tareasDB = leerInfo()
if(tareasDB){

  personas.cargarTareasFormArray(tareasDB);

  personas._listado = tareasDB

}

const personaGet = (req, res = response) => {

  const lista = leerInfo();

  res.json({
    msg: 'post API - el get funciona',
    lista,
  });
};

const personaPut =  (req = request, res = response) => {
  const  {id} = req.params;
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const index = personas._listado.findIndex(object => object.id === id)

  if (index !== -1){
    personas.editarPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      msg: 'persona editada - put funciona',
    })
  } else {
    res.json ({
      msg: 'person no encontrada - salida else',
    })
  }

};

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona( nombres, apellidos, ci, direccion, sexo )
  console.log('3333', persona, '333');

  personas.crearPersona(persona)
  console.log('000',personas._listado, '000');
  guardarInfo(personas._listado)

  
  res.json({
    msg: 'persona Creada - post funciona',
  });


  
};
const personaDelete = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id);
    guardarInfo(personas.personasArr)

    res.json({
    msg: 'persona eliminada - delete funciona',
    })

  }

};

const personaBuscarName = (req = request, res = response) =>{
  const {nombres, apellidos} = req.query;
  console.log(req.query);

  const posicion = personas._listado.findIndex(object => object.nombres === nombres && object.apellidos === apellidos)

  const dato = personas._listado[posicion]
 console.log(dato);

  if (posicion !== -1){
      res.json({
        msg: 'persona encontrada - buscadorName funciona',
        dato
      })
    } else {
      res.json ({
        msg: 'persona no encontrada',
      })
    }
}

const personaBuscarCI = (req = request, res = response) =>{
  const {ci} = req.query;
  console.log(parseInt(ci));
  const CI = parseInt(ci)
  const posicion = personas._listado.findIndex(object => object.ci === CI) 
  console.log(posicion);

  const dato = personas._listado[posicion]

  if (posicion !== -1){
    res.json({
      msg: 'persona encontrada',
      dato
    })
  } else {
    res.json ({
      msg: 'persona no encontrada',
    })
  }
}

const personaBuscarSexo = (req = request, res = response) =>{
  const {sexo} = req.query;

  let personas_sexo = personas._listado.filter(element => element.sexo === sexo);;
  res.json({
    personas_sexo
 });
}
const personaBuscarAs =(req = request, res = response
  ) => {
    const {obt, genero} = req.params;
    console.log(obt);
    
   
    const persona = personas._listado.filter(object => object.sexo === genero && object.nombres.includes(obt) === obt  )

    console.log(persona);
    
    
    if (persona){
      res.json({
        msg: 'persona encontrada',
        persona
      })
    } else {
      res.json ({
        msg: 'persona no encontrada',
      })
    }
}
  
module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete,
  personaBuscarName, 
  personaBuscarCI,
  personaBuscarSexo,
  personaBuscarAs
}




