const { Router } = require('express');
const { personaGet, personaPut, personaPost, personaDelete, personaBuscarName, personaBuscarCI, personaBuscarSexo,  personaBuscarAs } = require('../controllers/personaController');

const router = Router();

router.get('/', personaGet);

router.put('/:id', personaPut);

router.post('/', personaPost);

router.delete('/:id', personaDelete);

router.get('/buscar', personaBuscarName);

router.get('/CI', personaBuscarCI);

router.get('/sexo', personaBuscarSexo);

router.get('/:obt/:genero',personaBuscarAs)



module.exports = router;